const bool isProduction = bool.fromEnvironment('dart.vm.product');

const testConfig = {
  'apiUrl': 'http://10.0.2.2:5000/api/v1',
};

const productionConfig = {
  'apiUrl': 'http://10.0.2.2:5000/api/v1',
};

final env = isProduction ? productionConfig : testConfig;