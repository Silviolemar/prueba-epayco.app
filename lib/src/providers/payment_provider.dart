import 'package:http/http.dart' as http;
import '../models/Payment.dart';
import 'dart:async';
import 'dart:io';
import 'package:contador/.env.dart';

  Future<http.Response> sendMail(Payment payment) async{
    var url = '${env['apiUrl']}/payments/sendMail';
    final response = await http.post('$url',
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.authorizationHeader : payment.headers
        },
        body: paymentToJson(payment)
    );
    return response;
  }

  Future<http.Response> savePayment(Payment payment) async{
    var url = '${env['apiUrl']}/payments/savePayment';
    final response = await http.post('$url',
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.authorizationHeader : payment.headers
        },
        body: paymentToJson(payment)
    );
    return response;
  }