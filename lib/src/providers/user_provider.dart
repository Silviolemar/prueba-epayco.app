import 'package:http/http.dart' as http;
import '../models/User.dart';
import 'dart:async';
import 'dart:io';
import 'package:contador/.env.dart';

  Future<http.Response> loginUser(User user) async{
    var url = '${env['apiUrl']}/users/login';
    final response = await http.post('$url',
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.authorizationHeader : ''
        },
        body: userToJson(user)
    );
    return response;
  }

    Future<http.Response> createUser(User user) async{
    var url = '${env['apiUrl']}/users/create';
    final response = await http.post('$url',
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.authorizationHeader : ''
        },
        body: userToJson(user)
    );
    return response;
  }