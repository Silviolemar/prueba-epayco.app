import 'package:http/http.dart' as http;
import '../models/Wallet.dart';
import 'dart:async';
import 'dart:io';
import 'package:contador/.env.dart';

  Future<http.Response> chargeWallet(Wallet wallet) async{
    var url = '${env['apiUrl']}/wallets/charge';
    final response = await http.post('$url',
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.authorizationHeader : wallet.headers
        },
        body: walletToJson(wallet)
    );
    return response;
  }