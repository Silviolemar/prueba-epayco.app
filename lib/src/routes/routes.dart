import 'package:flutter/material.dart';
import 'package:contador/src/pages/login_page.dart';
import 'package:contador/src/pages/register_page.dart';

Map<String, WidgetBuilder> getRoutes(){

  return <String, WidgetBuilder>{
    '/'           : (BuildContext context) => LoginPage(), 
    'register'    : (BuildContext context) => RegisterPage(), 
  };
}
