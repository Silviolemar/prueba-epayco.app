import 'package:flutter/material.dart';
import 'package:contador/src/providers/user_provider.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:contador/src/models/User.dart';

class RegisterPage extends StatefulWidget {

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {

  String _email;
  String _password;
  String _document;
  String _name;
  String _phone;

  BuildContext _context;

  @override
  Widget build(BuildContext context) {

    _context = context;
    
    return Scaffold(
    backgroundColor: Color(0xFF222635),
    body: Container(
      padding: EdgeInsets.all(20.0),
      child: OrientationBuilder(
          builder: (BuildContext context, Orientation orientation) {
            return ListView(
                children: <Widget>[
                  _registerTittle(),
                  SizedBox(height: 50.0,),
                  _documentTittle(),
                  SizedBox(height: 10.0,),
                  _documentInput(),
                  SizedBox(height: 20.0,),
                  _nameTittle(),
                  SizedBox(height: 10.0,),
                  _nameInput(),
                  SizedBox(height: 20.0,),
                  _emailTittle(),
                  SizedBox(height: 10.0,),
                  _emailInput(),
                  SizedBox(height: 20.0,),
                  _passwordTittle(),
                  SizedBox(height: 10.0,),
                  _passwordInput(),
                  SizedBox(height: 20.0,),
                  _phoneTittle(),
                  SizedBox(height: 10.0,),
                  _phoneInput(),
                  SizedBox(height: 70.0,),
                  _acceptInput(),
              ],
            );
          }
        ),
      ),
    );
  }

  Widget _registerTittle() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          'Registro',
          style: TextStyle(fontSize: 30, color: Colors.white),
        ),
      ],
    );
  }

  Widget _documentTittle() {
    return Text(
      'Documento',
      style: TextStyle(color: Colors.white),
    );

  }

  Widget _documentInput() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Colors.white,
      ),
      child: TextField(
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
          border: InputBorder.none,
        ),
        onChanged: (value){
        setState(() {
            _document = value;
          });
        },
      ),
    );
  }

  Widget _nameTittle() {
    return Text(
      'Nombre',
      style: TextStyle(color: Colors.white),
    );
  }

  Widget _nameInput() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Colors.white,
      ),
      child: TextField(
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
          border: InputBorder.none,
        ),
        onChanged: (value){
        setState(() {
            _name = value;
          });
        },
      ),
    );
  }

  Widget _emailTittle() {
    return Text(
      'Email',
      style: TextStyle(color: Colors.white),
    );
  }

  Widget _emailInput() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Colors.white,
      ),
      child: TextField(
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
          border: InputBorder.none,
        ),
        onChanged: (value){
        setState(() {
            _email = value;
          });
        },
      ),
    );
  }

  Widget _phoneTittle() {
    return Text(
      'Celular',
      style: TextStyle(color: Colors.white),
    );
  }

  Widget _phoneInput() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Colors.white,
      ),
      child: TextField(
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
          border: InputBorder.none,
        ),
        onChanged: (value){
        setState(() {
            _phone = value;
          });
        },
      ),
    );
  }

  Widget _acceptInput() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 100.0),
      child: RaisedButton(
        padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 12.0),
        child: Text('Aceptar'),
        textColor: Colors.black,
        color: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0)
        ),
        onPressed: () => _registerUser()
      ),
    );
  }

    _registerUser(){ 

    _showAndHideLoading(true);

    User user = User(
        email: _email,
        password: _password,
        document: _document,
        name: _name,
        phone: _phone
    );

    createUser(user).then((response){

      _showAndHideLoading(false);      

      if(response.statusCode != 200){
        switch (response.statusCode) {
          case 500:
          print(response.body);
            if(response.body.contains('Path') || response.body.contains('salt'))
              _showToast('Campos erroneos');
            else
              _showToast('Error de conexión');
            break;
          default:
            print(response.body);
            if(response.body.contains('DUPLICATED_VALUES'))
              _showToast('Usuario ya se encuentra registrado');
            else
             _showToast('Error de conexión');
            break;
        }
      }else{
        Future.delayed(Duration(seconds: 1), () => Navigator.pushNamed(_context, 'home'));
        print(response.statusCode);

      }        
    }).catchError((error){
      print('error : $error');
    });
  }

    void _showAndHideLoading(bool _isVisible) {

    if(_isVisible){
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Dialog(
            backgroundColor: Colors.transparent,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                ),
              ],
            ),
          );
        },
      );
    }else{
      Future.delayed(Duration(seconds: 1), () => Navigator.pop(context));
    }
  }

  void _showToast(String msg){

    Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM
    );        

  }

  Widget _passwordTittle() {
    return Text(
      'Contraseña',
      style: TextStyle(color: Colors.white),
    );
  }

  Widget _passwordInput() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Colors.white,
      ),
      child: TextField(
        textCapitalization: TextCapitalization.sentences,
        obscureText: true,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
          border: InputBorder.none,
        ),
        onChanged: (value){
        setState(() {
            _password = value;
          });
        },
      ),
    );
  }
}