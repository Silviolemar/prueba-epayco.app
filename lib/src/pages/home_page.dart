import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:contador/src/models/Wallet.dart';
import 'package:contador/src/providers/wallet_provider.dart';
import 'dart:convert';
import 'package:contador/src/models/Payment.dart';
import 'package:contador/src/providers/payment_provider.dart';

class HomePage extends StatefulWidget {

  final String userData;

  HomePage(this.userData, {Key key}): super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  String _userName = '';
  String _amountT = '\$0.00';
  String _amountTUpdate = '';
  String _walletName = 'Billetera';
  String _document = '';
  String _phone = '';
  String _value = '';
  String _orderNumber = '';
  String _valueOrder = '';
  String _verifyCode = '';
  String _verifyCodeResponse = '';
  BuildContext _context;

  @override
  Widget build(BuildContext context) {

    _userName = json.decode(widget.userData)['data']['userName'];
    _walletName = json.decode(widget.userData)['data']['wallet']['name'];
    if(_amountTUpdate == ''){
      _amountT = '\$${json.decode(widget.userData)['data']['wallet']['value']}';
    }else{
       _amountT = _amountTUpdate;
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF222635),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Text(_userName),
          ],
        ),
      ),
      body: Container(
      child: OrientationBuilder(
          builder: (BuildContext context, Orientation orientation) {

            _context = context;

            return ListView(
                children: <Widget>[
                Column(
                  children: <Widget>[
                    _amountTotalTittle(),
                    SizedBox(height: 10.0,),
                    _amountTotal(),
                    SizedBox(height: 30.0,),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 5.0),
                      child: Divider(
                        color: Colors.black,
                      ),
                    ),
                    SizedBox(height: 10.0,),
                    _cardOptions()
                  ]
                )
              ],
            );
          }
        ),

      ),
    );
  }

  Widget _amountTotalTittle() {
    return Container(
      padding: EdgeInsets.only(top: 20.0,right: 20.0,left: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Text(_walletName),
          Expanded(child: SizedBox()),
          Text('Balance total'),
        ],
      ),
    );
  }

  Widget _amountTotal() {
    return Container(
      padding: EdgeInsets.only(right: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Text(
            _amountT,
            style: TextStyle(fontSize: 20.0),
          ),
        ],
      ),
    );
  }

  Widget _cardOptions() {    
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 40.0),
      child: Column(
        children: <Widget>[
          _chargeWalletOption(),
          SizedBox(height: 10.0,),
          _payOption(),
        ],
      ),
    );
  }

  Widget _chargeWalletOption() {
    return Card(
      elevation: 10.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      child: Container(
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: Color(0xFF222635),
        ),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Icon(
                  Icons.add_circle,
                  color: Colors.white,
                ),
              ],
            ),
            ListTile(
              contentPadding: EdgeInsets.only(bottom: 40.0, top: 20.0),
              title: Container(
                child: Center(
                  child: Text(
                    'Recargar billetera',
                    style: TextStyle(color: Colors.white),
                  )
                ),
              ),
              onTap: () => _chargeWalletAlert(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _payOption() {
    return Card(
      elevation: 10.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      child: Container(
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: Color(0xFF222635),
        ),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Icon(
                  Icons.attach_money,
                  color: Colors.white,
                ),
              ],
            ),
            ListTile(
              contentPadding: EdgeInsets.only(bottom: 40.0, top: 20.0),
              title: Container(
                child: Center(
                  child: Text(
                    'Pagar compra',
                    style: TextStyle(color: Colors.white),
                  )
                ),
              ),
              onTap: () => _payOptionAlert(),
            ),
          ],
        ),
      ),
    );
  }

  void _chargeWalletAlert() {

    showDialog(
      context: _context,
      barrierDismissible: true,
      builder: (_context){
        
        return Container(
          child: OrientationBuilder(
            builder: (BuildContext context, Orientation orientation) {
              return ListView(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 100.0),
                    child: AlertDialog(
                      title: Text('Recargar billetera'),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
                      content: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              _documentInputTittle()
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(vertical: 10.0), 
                            child: _documentInput()
                          ),
                          SizedBox(height: 10.0,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              _phoneInputTittle()
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(vertical: 10.0), 
                            child: _phoneInput()
                          ),
                           SizedBox(height: 10.0,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              _valueInputTittle()
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(vertical: 10.0), 
                            child: _valueInput()
                          ),
                        ],
                      ),
                      actions: <Widget>[
                        FlatButton(
                          child: Text('Cancelar'),
                          onPressed: () {
                            _resetInputs();
                            Navigator.pop(_context);
                          } 
                        ),
                        FlatButton(
                          child: Text('Aceptar'),
                          onPressed: () => _chargeWallet(),
                        ),
                      ],
                    ),
                  ),
                ],
              );
            }
          )
        );
      }
    );
  }

  Widget _documentInputTittle() {
    return Text('Documento');
  }

  Widget _documentInput() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Colors.grey[300],
      ),
      child: TextField(
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
          border: InputBorder.none,
        ),
        onChanged: (value){
          setState(() {
            _document = value;
          });
        },
      ),
    );
  }

  Widget _phoneInputTittle() {
    return Text('Celular');
  }

  Widget _phoneInput() {
        return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Colors.grey[300],
      ),
      child: TextField(
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
          border: InputBorder.none,
        ),
        onChanged: (value){
          setState(() {
            _phone = value;
          });
        },
      ),
    );
  }

  Widget _valueInputTittle() {
    return Text('Valor');
  }

  Widget _valueInput() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Colors.grey[300],
      ),
      child: TextField(
        textCapitalization: TextCapitalization.sentences,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
          border: InputBorder.none,
        ),
        onChanged: (value){
          setState(() {
            _value = value;
          });
        },
      ),
    );
  }

  _chargeWallet() {

    _showAndHideLoading(true);

    Wallet wallet = Wallet(
        headers: json.decode(widget.userData)['data']['token'],
        document: _document,
        phone: _phone,
        value: _value
    );

    chargeWallet(wallet).then((response){

      _showAndHideLoading(false);      

      if(response.statusCode != 200){
        print(response.body);
        switch (response.statusCode) {
          case 500:
            if(response.body.contains('jwt expired')){
              _showToast('Su sesion ha expirado');
              Future.delayed(Duration(seconds: 1), () => Navigator.pushNamed(_context, '/'));
            }else{
              _showToast('Error de conexión');
            }            
            break;
          default:
            _showToast('Campos incorrectos');
            break;
        }
      }else{
        _showToast('Recarga exitosa');
        Future.delayed(Duration(seconds: 1), () {
          Navigator.pop(_context);
          setState(() => _amountTUpdate = '\$${json.decode(response.body)['data']['sumValue']}');
        });

        _resetInputs();   
      }        
    }).catchError((error){
      print('error : $error');
    });
  }

  _sendMail() {

    if(_valueOrder != ''){
      if(double.parse(_valueOrder) > json.decode(widget.userData)['data']['wallet']['value']){
        _showToast('Saldo insuficiente');
        return;
      }
    }

    _showAndHideLoading(true);

    Payment payment = Payment(
        headers: json.decode(widget.userData)['data']['token'],
        orderNumber: _orderNumber,
        amount: _valueOrder,
        userEmail: json.decode(widget.userData)['data']['email']
    );

    sendMail(payment).then((response){

      _showAndHideLoading(false);      

      if(response.statusCode != 200){
        print(response.body);
        switch (response.statusCode) {
          case 500:
            if(response.body.contains('jwt expired')){
              _showToast('Su sesion ha expirado');
              Future.delayed(Duration(seconds: 1), () => Navigator.pushNamed(_context, '/'));
            }else{
              _showToast('Error de conexión');
            }            
            break;
          default:
            _showToast('Campos incorrectos');
            break;
        }
      }else{
        _showToast('Se envio un codigo de verificacion a ${json.decode(widget.userData)['data']['email']}');
        Future.delayed(Duration(seconds: 1), () {
          Navigator.pop(_context);
          _verifyCodeResponse = json.decode(response.body)['data']['randToken'];
          print(_verifyCodeResponse);
          _paymentConfirmAlert();
        });       
      }        
    }).catchError((error){
      print('error : $error');
    });
  }

  void _showAndHideLoading(bool _isVisible) {

    if(_isVisible){
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Dialog(
            backgroundColor: Colors.transparent,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                ),
              ],
            ),
          );
        },
      );
    }else{
      Future.delayed(Duration(seconds: 1), () => Navigator.pop(context));
    }
  }

  void _showToast(String msg){

    Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM
    );        

  }

  void _payOptionAlert() {
    showDialog(
      context: _context,
      barrierDismissible: true,
      builder: (_context){
        
        return Container(
          child: OrientationBuilder(
            builder: (BuildContext context, Orientation orientation) {
              return ListView(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 100.0),
                    child: AlertDialog(
                      title: Text('Simulación compra'),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
                      content: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              _orderNumberInputTittle()
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(vertical: 10.0), 
                            child: _orderNumberInput()
                          ),
                          SizedBox(height: 10.0,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              _valueInputTittle()
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(vertical: 10.0), 
                            child: _valueOrderInput()
                          ),
                        ],
                      ),
                      actions: <Widget>[
                        FlatButton(
                          child: Text('Cancelar'),
                          onPressed: (){
                            _resetInputs();
                            Navigator.pop(_context);
                          } 
                        ),
                        FlatButton(
                          child: Text('Aceptar'),
                          onPressed: () => _sendMail(),
                        ),
                      ],
                    ),
                  ),
                ],
              );
            }
          )
        );
      }
    );
  }

  Widget _orderNumberInputTittle() {
    return Text('Numero de orden');
  }

  Widget _orderNumberInput() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Colors.grey[300],
      ),
      child: TextField(
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
          border: InputBorder.none,
        ),
        onChanged: (value){
          setState(() {
            _orderNumber = value;
          });
        },
      ),
    );
  }

  Widget _valueOrderInput() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Colors.grey[300],
      ),
      child: TextField(
        textCapitalization: TextCapitalization.sentences,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
          border: InputBorder.none,
        ),
        onChanged: (value){
          setState(() {
            _valueOrder = value;
          });
        },
      ),
    );
  }

  void _resetInputs() {
    _document = '';
    _phone = '';
    _value = '';
    _orderNumber = '';
    _valueOrder = '';
    _verifyCode = '';
  }

  void _paymentConfirmAlert() {
    showDialog(
      context: _context,
      barrierDismissible: true,
      builder: (_context){
        
        return Container(
          child: OrientationBuilder(
            builder: (BuildContext context, Orientation orientation) {
              return ListView(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 100.0),
                    child: AlertDialog(
                      title: Text('Confirmar pago'),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
                      content: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              _verifyCodeTittle()
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(vertical: 10.0), 
                            child: _verifyCodeInput()
                          ),
                        ],
                      ),
                      actions: <Widget>[
                        FlatButton(
                          child: Text('Cancelar'),
                          onPressed: (){
                            _resetInputs();
                            Navigator.pop(_context);
                          } 
                        ),
                        FlatButton(
                          child: Text('Confirmar'),
                          onPressed: () => _verifyCodeCallApi(),
                        ),
                      ],
                    ),
                  ),
                ],
              );
            }
          )
        );
      }
    );
  }

  Widget _verifyCodeTittle() {
    return Text('Codigo de verificación');
  }

  Widget _verifyCodeInput() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Colors.grey[300],
      ),
      child: TextField(
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
          border: InputBorder.none,
        ),
        onChanged: (value){
          setState(() {
            _verifyCode = value;
          });
        },
      ),
    );
  }

  _verifyCodeCallApi() {

    _showAndHideLoading(true);

    if(_verifyCode != _verifyCodeResponse){
      _showToast('Codigo incorrecto');
      Future.delayed(Duration(seconds: 1), () {
          Navigator.pop(_context);
          _verifyCode = '';
        });   
      return;
    }

    Payment payment = Payment(
      headers: json.decode(widget.userData)['data']['token'],
      orderNumber: _orderNumber,
      amount: _valueOrder,
      walletId: json.decode(widget.userData)['data']['wallet']['_id']
    );

    savePayment(payment).then((response){

      _showAndHideLoading(false);      

      if(response.statusCode != 200){
        print(response.body);
        switch (response.statusCode) {
          case 500:
            if(response.body.contains('jwt expired')){
              _showToast('Su sesion ha expirado');
              Future.delayed(Duration(seconds: 1), () => Navigator.pushNamed(_context, '/'));
            }else{
              _showToast('Error de conexión');
            }            
            break;
          default:
            _showToast('Campos incorrectos');
            break;
        }
      }else{
        _showToast('Pago exitoso');
        Future.delayed(Duration(seconds: 1), () {
          Navigator.pop(_context);
          _verifyCodeResponse = '';
          _resetInputs();
          setState(() => _amountTUpdate = '\$${json.decode(response.body)['data']['subtractionValue']}');
        });       
      }        
    }).catchError((error){
      print('error : $error');
    });
  }
}