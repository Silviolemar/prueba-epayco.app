import 'package:contador/src/pages/home_page.dart';
import 'package:flutter/material.dart';
import '../providers/user_provider.dart';
import 'package:contador/src/models/User.dart';
import 'package:fluttertoast/fluttertoast.dart';

class LoginPage extends StatefulWidget {

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  String _email;
  String _password;
  BuildContext _context;

  @override
  Widget build(BuildContext context) {

    _context = context;

    return Scaffold(
    backgroundColor: Color(0xFF222635),
    body: Container(
      child: OrientationBuilder(
          builder: (BuildContext context, Orientation orientation) {
            return ListView(
                children: <Widget>[
                _imageHeader(),
                SizedBox(),
                Container(
                  padding: EdgeInsets.only(left: 50.0, right: 50.0, top: 50.0),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          _inputUserTittle(),
                        ],
                      ),
                      SizedBox(height: 10.0,),
                      _inputUser(),
                      SizedBox(height: 30.0,), 
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          _inputPasswordTittle(),
                        ],
                      ),
                      SizedBox(height: 10.0,),
                      _inputPassword(),
                      SizedBox(height: 30.0,),
                      _registerButtom(),
                      SizedBox(height: 10.0,),
                      _loginButtom()
                    ]
                  ),
                )
              ],
            );
          }
        ),

      ),
    );
  }

  Widget _imageHeader() {

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 100.0),
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Epayco Wallet',
                style: TextStyle(fontSize: 50),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget _inputUser() {

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Colors.white,
      ),
      child: TextField(
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
          border: InputBorder.none,
        ),
        onChanged: (value){
          setState(() {
            _email = value;
          });
        },
      ),
    );
  }

  Widget _inputUserTittle() {
    return Text(
      'Email',
      style: TextStyle(color: Colors.white),
    );
  }

  Widget _inputPasswordTittle() {
    return Text(
      'Contraseña',
      style: TextStyle(color: Colors.white),
    );
  }

  Widget _inputPassword() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Colors.white,
      ),
      child: TextField(
        textCapitalization: TextCapitalization.sentences,
        obscureText: true,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
          border: InputBorder.none,
        ),
        onChanged: (value){
        setState(() {
            _password = value;
          });
        },
      ),
    );
  }

  Widget _loginButtom() {

    return RaisedButton(
      padding: EdgeInsets.symmetric(horizontal: 50.0, vertical: 12.0),
      child: Text('Aceptar'),
      textColor: Colors.black,
      color: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0)
      ),
      onPressed: () => _login()
    );

  }

  _login() { 

    _showAndHideLoading(true);

    User login = User(
        email: _email,
        password: _password
    );

    loginUser(login).then((response) {

      _showAndHideLoading(false);      

      if(response.statusCode != 200){
        switch (response.statusCode) {
          case 500:
            _showToast('Error de conexión');
            break;
          default:
            _showToast('Usuario y contraseña no coinciden');
            break;
        }
      }else{
        Future.delayed(Duration(seconds: 1), () => _navigateHomePage(response.body));
        print(response.body);

      }        
    }).catchError((error){
      print('error : $error');
    });
  }

  void _showAndHideLoading(bool _isVisible) {

    if(_isVisible){
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Dialog(
            backgroundColor: Colors.transparent,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                ),
              ],
            ),
          );
        },
      );
    }else{
      Future.delayed(Duration(seconds: 1), () => Navigator.pop(context));
    }
  }

  void _showToast(String msg){

    Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM
    );        

  }

  Widget _registerButtom() {

    return RaisedButton(      
      padding: EdgeInsets.symmetric(horizontal: 40.0, vertical: 12.0),
      child: Text('Registrarse'),
      textColor: Colors.black,
      color: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0)
      ),
      onPressed: () => Navigator.pushNamed(_context, 'register')
    );
  }

  void _navigateHomePage(String userData) async {

    await Navigator.push(
      _context,
      MaterialPageRoute(
        builder: (_context) => HomePage(userData),
    ));
  }
}