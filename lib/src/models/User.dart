import 'dart:convert';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
    User({
        this.email,
        this.password,
        this.document,
        this.name,
        this.phone,
    });

    String email;
    String password;
    String document;
    String name;
    String phone;

    factory User.fromJson(Map<String, dynamic> json) => User(
        email: json["email"],
        password: json["password"],
        document: json["document"],
        name: json["name"],
        phone: json["phone"],
    );

    Map<String, dynamic> toJson() => {
        "email": email,
        "password": password,
        "document": document,
        "name": name,
        "phone": phone,
    };
}