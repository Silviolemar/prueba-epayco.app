import 'dart:convert';

Payment paymentFromJson(String str) => Payment.fromJson(json.decode(str));
String paymentToJson(Payment data) => json.encode(data.toJson());

class Payment {
    Payment({
        this.headers,
        this.orderNumber,
        this.amount,
        this.userEmail,
        this.walletId,
    });

    String headers;
    String orderNumber;
    String amount;
    String userEmail;
    String walletId;

    factory Payment.fromJson(Map<String, dynamic> json) => Payment(
        headers: json["headers"],
        orderNumber: json["orderNumber"],
        amount: json["amount"],
        userEmail: json["userEmail"],
        walletId: json["walletId"],
    );

    Map<String, dynamic> toJson() => {
        "headers": headers,
        "orderNumber": orderNumber,
        "amount": amount,
        "userEmail": userEmail,
        "walletId": walletId,
    };
}