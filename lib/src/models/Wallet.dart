import 'dart:convert';

Wallet walletFromJson(String str) => Wallet.fromJson(json.decode(str));
String walletToJson(Wallet data) => json.encode(data.toJson());

class Wallet {
    Wallet({
        this.headers,
        this.name,
        this.document,
        this.phone,
        this.value,
    });

    String headers;
    String name;
    String document;
    String phone;
    String value;

    factory Wallet.fromJson(Map<String, dynamic> json) => Wallet(
        headers: json["headers"],
        name: json["name"],
        document: json["document"],
        phone: json["phone"],
        value: json["value"],
    );

    Map<String, dynamic> toJson() => {
        "headers": headers,
        "name": name,
        "document": document,
        "phone": phone,
        "value": value,
    };
}