# prueba-epayco.app

A new Flutter project.

Getting Started
This project is a starting point for a Flutter application.
A few resources to get you started if this is your first Flutter project:

Lab: Write your first Flutter app
Cookbook: Useful Flutter samples

For help getting started with Flutter, view our
online documentation, which offers tutorials,
samples, guidance on mobile development, and a full API reference.

# step to generate the apk

1. configure the .env.dart file by changing the url to refer to your server.
2. run flutter build apk command
3. the apk will be generated in the /prueba-epayco.app/build/app/outputs/apk/release/app-release.apk folder

For more information see https://flutter-es.io/docs/deployment/android

